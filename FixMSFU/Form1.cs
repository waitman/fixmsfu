﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace FixMSFU
{
    public partial class Form1 : Form
    {
        RegistryKey baseRegistryKey;
        public Form1()
        {
            InitializeComponent();
            baseRegistryKey = Registry.LocalMachine;

            listView1.Columns.Add("Device ID", 250, HorizontalAlignment.Left);
            listView1.Columns.Add("Device Name", 250, HorizontalAlignment.Left);
            listView1.Columns.Add("Device Description", 250, HorizontalAlignment.Left);
            listView1.Columns.Add("Location", 250, HorizontalAlignment.Left);
            listView1.Columns.Add("MFG", 150, HorizontalAlignment.Left);
            refreshDevices();

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            refreshDevices();
        }

        private void refreshDevices()
        {
            listView1.Items.Clear();
            int SubItemIndex = 0;

            RegistryKey rk = baseRegistryKey;
            RegistryKey sk = rk.OpenSubKey("SYSTEM\\ControlSet001\\Enum\\SWD\\DAFUPnPProvider", true);
            Console.WriteLine("count: " + sk.SubKeyCount);

            foreach (var value in sk.GetSubKeyNames())
            {
                RegistryKey nk = sk.OpenSubKey(value);

                try
                {
                    string friendlyname = nk.GetValue("FriendlyName").ToString();
                    listView1.Items.Add(value, nk.Name);
                    listView1.Items[SubItemIndex].Tag = nk.Name;
                    listView1.Items[SubItemIndex].SubItems.Add(friendlyname);
                    listView1.Items[SubItemIndex].SubItems.Add(nk.GetValue("DeviceDesc").ToString());
                    listView1.Items[SubItemIndex].SubItems.Add(nk.GetValue("LocationInformation").ToString());
                    listView1.Items[SubItemIndex].SubItems.Add(nk.GetValue("MFG").ToString());
                    SubItemIndex += 1;
                } catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            TokenManipulator.AddPrivilege("SeRestorePrivilege");
            TokenManipulator.AddPrivilege("SeBackupPrivilege");
            TokenManipulator.AddPrivilege("SeTakeOwnershipPrivilege");

            RegistryKey rk = baseRegistryKey;
            RegistryKey sk = rk.OpenSubKey("SYSTEM\\ControlSet001\\Enum\\SWD\\DAFUPnPProvider", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.TakeOwnership);
            int delCount = 0;

            ListView.SelectedListViewItemCollection lvSel = this.listView1.SelectedItems;
            string strKeyName = null; 
            foreach (ListViewItem lvItem in lvSel) 
            {

                strKeyName = lvItem.Text.ToString();
                try 
                {
                    RegistryKey nk = sk.OpenSubKey(strKeyName, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.TakeOwnership);
                    RegistryKey gk = nk.OpenSubKey("Properties", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.TakeOwnership);
                    nk.DeleteSubKeyTree("Properties");
                    sk.DeleteSubKeyTree(strKeyName);
                    delCount++;
                } 
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + " (Key: " +strKeyName + ")"); 

                }
            }

            if (delCount > 0)
            {
                MessageBox.Show(delCount.ToString() + " devices deleted.");
                refreshDevices();
            }

        }
    }


    public class TokenManipulator
    {


        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall,
        ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);


        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GetCurrentProcess();


        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr
        phtok);


        [DllImport("advapi32.dll", SetLastError = true)]
        internal static extern bool LookupPrivilegeValue(string host, string name,
        ref long pluid);


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct TokPriv1Luid
        {
            public int Count;
            public long Luid;
            public int Attr;
        }

        internal const int SE_PRIVILEGE_DISABLED = 0x00000000;
        internal const int SE_PRIVILEGE_ENABLED = 0x00000002;
        internal const int TOKEN_QUERY = 0x00000008;
        internal const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;

        public const string SE_ASSIGNPRIMARYTOKEN_NAME = "SeAssignPrimaryTokenPrivilege";
        public const string SE_AUDIT_NAME = "SeAuditPrivilege";
        public const string SE_BACKUP_NAME = "SeBackupPrivilege";
        public const string SE_CHANGE_NOTIFY_NAME = "SeChangeNotifyPrivilege";
        public const string SE_CREATE_GLOBAL_NAME = "SeCreateGlobalPrivilege";
        public const string SE_CREATE_PAGEFILE_NAME = "SeCreatePagefilePrivilege";
        public const string SE_CREATE_PERMANENT_NAME = "SeCreatePermanentPrivilege";
        public const string SE_CREATE_SYMBOLIC_LINK_NAME = "SeCreateSymbolicLinkPrivilege";
        public const string SE_CREATE_TOKEN_NAME = "SeCreateTokenPrivilege";
        public const string SE_DEBUG_NAME = "SeDebugPrivilege";
        public const string SE_ENABLE_DELEGATION_NAME = "SeEnableDelegationPrivilege";
        public const string SE_IMPERSONATE_NAME = "SeImpersonatePrivilege";
        public const string SE_INC_BASE_PRIORITY_NAME = "SeIncreaseBasePriorityPrivilege";
        public const string SE_INCREASE_QUOTA_NAME = "SeIncreaseQuotaPrivilege";
        public const string SE_INC_WORKING_SET_NAME = "SeIncreaseWorkingSetPrivilege";
        public const string SE_LOAD_DRIVER_NAME = "SeLoadDriverPrivilege";
        public const string SE_LOCK_MEMORY_NAME = "SeLockMemoryPrivilege";
        public const string SE_MACHINE_ACCOUNT_NAME = "SeMachineAccountPrivilege";
        public const string SE_MANAGE_VOLUME_NAME = "SeManageVolumePrivilege";
        public const string SE_PROF_SINGLE_PROCESS_NAME = "SeProfileSingleProcessPrivilege";
        public const string SE_RELABEL_NAME = "SeRelabelPrivilege";
        public const string SE_REMOTE_SHUTDOWN_NAME = "SeRemoteShutdownPrivilege";
        public const string SE_RESTORE_NAME = "SeRestorePrivilege";
        public const string SE_SECURITY_NAME = "SeSecurityPrivilege";
        public const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
        public const string SE_SYNC_AGENT_NAME = "SeSyncAgentPrivilege";
        public const string SE_SYSTEM_ENVIRONMENT_NAME = "SeSystemEnvironmentPrivilege";
        public const string SE_SYSTEM_PROFILE_NAME = "SeSystemProfilePrivilege";
        public const string SE_SYSTEMTIME_NAME = "SeSystemtimePrivilege";
        public const string SE_TAKE_OWNERSHIP_NAME = "SeTakeOwnershipPrivilege";
        public const string SE_TCB_NAME = "SeTcbPrivilege";
        public const string SE_TIME_ZONE_NAME = "SeTimeZonePrivilege";
        public const string SE_TRUSTED_CREDMAN_ACCESS_NAME = "SeTrustedCredManAccessPrivilege";
        public const string SE_UNDOCK_NAME = "SeUndockPrivilege";
        public const string SE_UNSOLICITED_INPUT_NAME = "SeUnsolicitedInputPrivilege";

        public static bool AddPrivilege(string privilege)
        {
            try
            {
                bool retVal;
                TokPriv1Luid tp;
                IntPtr hproc = GetCurrentProcess();
                IntPtr htok = IntPtr.Zero;
                retVal = OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
                tp.Count = 1;
                tp.Luid = 0;
                tp.Attr = SE_PRIVILEGE_ENABLED;
                retVal = LookupPrivilegeValue(null, privilege, ref tp.Luid);
                retVal = AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static bool RemovePrivilege(string privilege)
        {
            try
            {
                bool retVal;
                TokPriv1Luid tp;
                IntPtr hproc = GetCurrentProcess();
                IntPtr htok = IntPtr.Zero;
                retVal = OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
                tp.Count = 1;
                tp.Luid = 0;
                tp.Attr = SE_PRIVILEGE_DISABLED;
                retVal = LookupPrivilegeValue(null, privilege, ref tp.Luid);
                retVal = AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
