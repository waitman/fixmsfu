
This program was developed because I noticed thousands of UPnP devices appearing in my "Control Panel/Hardware and Sound/Devices and Printers" The same devices were duplicated over and over. If you click on an icon in that list, you can "Remove" it, but if you select more than one the "Remove" button goes away. I don't know if having thousands of devices in the list messes up or slows down the computer, but it sounds like a bad idea anyway. So this program will give a list of all the UPnP devices and you can easily select multiple devices and delete them in one bulk operation.


If you want to delete a gadzillion UPnP devices as a bulk delete, download psexec and run the enclosed fixmsfu program.

(see Screenshots)


PsExec - Windows Sysinternals

https://docs.microsoft.com/en-us/sysinternals/downloads/psexec

PsExec is a program intended to run processes on remote MS Windows hosts, but if you omit the host it will default to the local machine. This program lets you execute the specified program as the "system" account. 

The easy way is to copy PsExec.exe / PsExec64.exe to bin/Release then run 'cmd.exe' as Administrator, 
when you run PsExec(64).exe you have to specify the full path to the program you want to run.

example:

```
PsExec64.exe -s -i -d "C:\Users\waitm\OneDrive\Documents\Visual Studio 2017\Projects\FixMSFU\FixMSFU\bin\Release\FixMSFU.exe"
```

When the FixMSFU program launches, you can select multiple files using shift/control key. Don't need to check the boxes. When you hit delete button it will remove the selected UPnP devices from the device list (in Control Panel\Hardware and Sound\Devices and Printers). 

Note that the list is cached, AFAIK you have to REBOOT in order to update the cached entries. After reboot the list should be cleaned up. To stop the madness of devices multiplying, you can disable UPnP / "Network Sharing"



